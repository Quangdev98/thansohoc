$('#discover .wrap-silde .owl-carousel').owlCarousel({
	loop: true,
	nav: true,
	dots: true,
	autoplay: false,
	autoplayTimeout: 5000,
	autoplayHoverPause: true,
	navText:[`<img src="../images/prev.png" />`, `<img src="../images/next.png" />`],
	responsive: {
		0: {
			items: 1,
			margin: 15,
		},
		768: {
			items: 2,
			margin: 15,
		}, 1024: {
			items: 2,
			margin: 20,
		}, 1080: {
			items: 2,
			margin: 36,
		}
	}
});

function resizeImage() {
	let arrClass = [
		{ class: 'resize-banner', number: (289 / 542) },
	];
	for (let i = 0; i < arrClass.length; i++) {
		if($("." + arrClass[i]['class']).length){
			let width = document.getElementsByClassName(arrClass[i]['class'])[0].offsetWidth;
			$("." + arrClass[i]['class']).css('height', width * arrClass[i]['number'] + 'px');
		}
	}
}

resizeImage();
new ResizeObserver(() => {
	resizeImage();
	
}).observe(document.body)






$(document).ready(function () {

	$(".table-chart .current-power").click(function() {
		$("#powerNumber").modal('show')
	})
	$("#get-otp").click(function() {
		$(".layout-step-1").hide()
		$(".layout-step-2").addClass('active')
		$("#input1").focus()
	})

	$(".industry-group .item:not(.active)").click(function () {
		$(this).addClass("active")
		let content = $(this).find('.number').attr('data-content');
		$(this).find('.number').css({'opacity': 0})
		setTimeout(() => {
			$(this).find('.number').css({'opacity': 1})
			$(this).find('.number').text(content)
		}, 300);
	})
	$('#file-avatar').change(function (e) {
		if (this.files && this.files[0]) {
			let url = URL.createObjectURL(e.target.files[0]);
			$(this).parent('.box-avatar').attr("style", "background: #eef0f8 url('" + url + "') no-repeat top center; background-size: cover; display: block; background-position: center");
			setTimeout(() => {
				$(".form-update-avatar").css({'display': "none"})
				$(".popup-avatar .result-form").css({'display': "block"})
			}, 200);
		}
	})

	$(window).on('scroll', function () {
		if ($(window).scrollTop()) {
			$('#header:not(.header-project-detail-not-scroll)').addClass('active');
		} else {
			$('#header:not(.header-project-detail-not-scroll)').removeClass('active')
		};
	});
});